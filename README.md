# Another Flavor Theme

The [hugo](https://gohugo.io/) theme for the [Another Flavor
blog](https://anotherflavor.peterdebelak.com/).

## Features

* HTML and AMP formats for posts
* Fingerprinted css for caching
* Open Graph and schema.org metadata

## Quick Start

```
# clone the theme into your themes directory
$ git clone https://gitlab.com/pdebelak/anotherflavor-theme.git themes/anotherflavor
```

Update your config.toml to include the following:

```
theme = "anotherflavor"

[params]
  subtitle = "My site's subtitle"
  description = "Super cool description"
  author = "My Name"
  image = "images/my-default-site-image.png"
  logo = "images/my-logo.png"

[outputs]
 page = ["HTML", "AMP"]
```

## Shortcodes

For amp to work, you need to use the shortcodes for images and youtube videos.

### Images:

```
{{<img src="/images/my-image" height="400" width="600">}}
```

Note that the height and width _are_ required for amp.

### Youtube videos

```
{{< yt src="SSbBvKaM6sk" height="315" width="420">}}
```
